const FOUR_HOURS = 16200;

new Vue({
  el: "#timer",
  data() {
    return {
      timer: FOUR_HOURS,
    };
  },
  filters: {
    hoursMinutesAndSeconds(value) {
      var hours = Math.floor(parseInt(value, 10) / 3600);
      var minutes = Math.floor((parseInt(value, 10) % 3600) / 60);
      var seconds = (parseInt(value, 10) % 3600) % 60;
      return `${hours} hours ${minutes} minutes ${seconds} seconds left`;
    },
  },
  mounted() {
    setInterval(() => {
      this.timer -= 1;
    }, 1000);
  },
});

new Vue({
  el: "#datePicker",
  data: {
    mode: "single",
    selectedDate: null,
    datetime: ""
  },
});

new Vue({
  el: "#stateBranch",
  data: {
    states: [
      {
        label: "State A",
        branches: ["Branch A", "Branch A1"],
      },
      {
        label: "State B",
        branches: ["Branch B", "Branch B1", "Branch B2", "Branch B3"],
      },
      {
        label: "State C",
        branches: ["Branch C"],
      },
      {
        label: "State D",
        branches: ["Branch D", "Branch D1", "Branch D2"],
      },
    ],

    selectedState: "",
    selectedBranch: "",
  },
});
